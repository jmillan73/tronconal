package com.curso;

import com.curso.model.Gato;

public class App {

    public static void main(String[] args){

        Gato gato1 = new Gato();
        gato1.setEdad(14);
        gato1.setDomestico(true);
        gato1.setNombre("Pelusa");
        System.out.println("Informacion del gato 1 "+ gato1.toString());
        
    }

}
