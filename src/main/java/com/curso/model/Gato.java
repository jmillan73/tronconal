package com.curso.model;

public class Gato {

    private String nombre;
    private int Edad;
    private boolean domestico;

    public Gato(String nombre, int edad, boolean domestico) {
        this.nombre = nombre;
        Edad = edad;
        this.domestico = domestico;
    }

    public Gato() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int edad) {
        Edad = edad;
    }

    public boolean isDomestico() {
        return domestico;
    }

    public void setDomestico(boolean domestico) {
        this.domestico = domestico;
    }
}
